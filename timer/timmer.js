let time
let start = 0
let pause = 0
let running = false
let counter = 0
let total_sec = 0
let output = $('.timer').find("span")


$('.start-btn').click(function startPause() {
    total_sec = convertToSec()
    if (running == false && total_sec != 0) {
        running = true
        timer()
        $('.start-btn').html('Pause')
    }
    else if(total_sec != 0) {
        running = false
        $('.start-btn').html('Resume')
    }
})

$('.reset-btn').click(function reset() {
    running = false
    counter = 0
    convertToTimeFormat(total_sec)
    $('.start-btn').html('Start')
})

let convertToSec = () => {
    time = $('.watch').val().split(':')
    let hour = parseInt(time[0])
    let min = parseInt(time[1])
    let sec = parseInt(time[2])
    total_sec = (hour * 3600) + (min * 60) + (sec * 1)
    return total_sec
}


function timer() {
    if (running) {
        setTimeout(() => {
            
            leftTime = total_sec - counter 
            convertToTimeFormat(leftTime) 
            counter++
            if (leftTime) {    
                timer()
            }
            else {
                alert('Time Up');
                $('.start-btn').html('start')
                running = false
                counter = 0
                total_sec = 0
            }

        }, 1000)
    }
}


let convertToTimeFormat = (total_sec) => {
    hour = Math.floor(total_sec / 3600)
    min = Math.floor((total_sec % 3600) / 60)
    sec = (total_sec % 3600) % 60
    // let output = $('.timer').find("span")
    output.eq(0).text(hour);
    output.eq(1).text(min);
    output.eq(2).text(sec);
}
