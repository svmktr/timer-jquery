var counter = 0
var running = 0

$('.start-btn').click(function startPause(){
    if(running == 0){
        running = 1
        increment()
        $('.start-btn').html('Pause')
    }
    else{
        running = 0
        $('.start-btn').html('Resume')
    }
})

$('.reset-btn').click(function reset(){
    running = 0
    counter = 0
    convertToTimeFormat(counter)
    lapCounter(counter)
    $('.start-btn').html('Start')
})

let increment = () =>{
    if(running){
        setTimeout(()=>{ 
            convertToTimeFormat(counter) 
            counter++
            increment()    
        }, 1000)
    }
}

let convertToTimeFormat = (counter) => {
    hour = Math.floor(counter / 3600)
    min = Math.floor((counter % 3600) / 60)
    sec = (counter % 3600) % 60
    let output = $('.timer').find("span")
    output.eq(0).text(hour);
    output.eq(1).text(min);
    output.eq(2).text(sec);
}

let start = 0
let end = 0
$('.lap-btn').click(()=>{  
    if(start == 0 && running){
        start = counter-1
        lapCounter(start)
    }
    else if(running){
        end = counter - 1
        lapCounter(end-start)
        start = end
        end = 0
    }
})


let lapCounter = (lap) => {
    hour = Math.floor(lap / 3600)
    min = Math.floor((lap % 3600) / 60)
    sec = (lap % 3600) % 60
    let output = $('.laps').find("span")
    output.eq(0).text(hour);
    output.eq(1).text(min);
    output.eq(2).text(sec);

}